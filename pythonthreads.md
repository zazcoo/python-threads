# to import the libraries
import threading
import time

# to create the function for thread 1
def ascending():
    count = 1
    while count<11:
        print(count)
        time.sleep(1)
        count += 1

# to create the function for thread 2
def descending():
    count = 10
    while count>0:
        print(count)
        time.sleep(1)
        count -= 1
        
# to create the main function
def main():
    thread_1 = threading.Thread(target = ascending)
    thread_2 = threading.Thread(target = descending)
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()

#to execute the main function
main()
